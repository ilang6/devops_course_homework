from PIL import Image, ImageDraw, ImageFont


# 1

a = 1/0

# 2

a = 1/0

try:
  a = 1/0
except ZeroDivisionError as e:
    print(e)

# 3

# yes the code is fine try can come with finally

# 4

# Except can handle all types of exceptions

# 5

# it wrong because it's not informative and dosn;t show the issue with the code

# 6

# IOError - errors with input and output
# ZeroDivisionError - when you try to divine a number by zero

# 7

file = open("/Users/ilangofer/devops_course_homework/ilan.txt",'w')
file.close()

# 8

file = open("/Users/ilangofer/devops_course_homework/ilan.txt",'w')
file.write("Ilan Gofer")
file.close()

# 9

file = open("/Users/ilangofer/devops_course_homework/ilan.txt",'r')
string = file.read()
print(string)
file.close()


# 10

file_heb = open("/Users/ilangofer/devops_course_homework/heb.txt", 'w', encoding='utf-8')
file_heb.write("שלום")
file_heb.close()

file_heb = open("/Users/ilangofer/devops_course_homework/heb.txt", 'r', encoding='utf-8')
string = file_heb.read()
print(string)

# 11

img = Image.new('RGB', (100, 30), color=(73, 109, 137))
fnt = ImageFont.truetype('/Library/Fonts/Arial.ttf', 15)
d = ImageDraw.Draw(img)
d.text((10, 10), "DevOps", font=fnt, fill=(255, 255, 0))
img.save('/Users/ilangofer/devops_course_homework/devops.png')
