
# A

first = 7
second = 44.3

print(first+second)  # = 51.3
print(first*second)  # = 310.09999999999997
print(second/first)  # = 6.328571428571428

# B

a = 8
a = 17
a = 9
b = 6
c = a+b
b = c+a
b = 8
print(a, b, c)

# the answer is (a=9 b=8 c=15)

# c

name = "john"
name = 'john'

# No both ways 
# are acceptable ways to use with string

my_number = 5+5
print("result is: "+my_number)

# you can't add a string to int
# the right way should be:"

print("result is: "+str(my_number))

# D

x = 5
y = 2.36
print(x+int(y))

# the answer is seven 
# because int is an number without a decimal point 
# and will disregard numbers after the decimal point 

# CHALLENGE

a = 8
b = "123"

# option A
print(a+int(b))

