# A

x = 600
y = 23

if x > y:
    print("BIG")
else:
    print("SMALL")

# B

for i in range(5):
    print(i)

# C

seasons = 1

if seasons == 1:
    print("summer")
elif seasons == 2:
    print("winter")
elif seasons == 3:
    print("fall")
elif seasons == 4:
    print("spring")

# D

count = 1
while count < 11:
    print(count)
    count = count+1

# will run 10 times
# the number 10 will be printed last

# E

my_dict = {"Age": 34,
           "FL": "G",
           "Currency": 3.58,
           "Abroad": True,
           "Apartment number": 9}

my_dict["Currency"]+my_dict["Age"]

# F

phonenum = input("input your phone number")
print("your phone number is"+' '+str(phonenum))

# G


def printHello():
    print("hello")


printHello()


def calculate():
    print(5+3.2)


calculate()

# H


def myname(n):
    print(n)


myname("ilan")


def divde(n):
    print(n/2)


divde(12)

# I

def sumtwo(x,y):
    print(x+y)

sumtwo(5,8)


def twostrings(x,y):
    print(x +' '+y)

twostrings("ilan","gofer")

# k

for num in range(6):
    for i in range(num):
        print ('*', end=" ")
    print("\n")

# L

i = 0
j = 6

for row in range(7):
    for col in range(7):
        if row == i and col == j:
            print('*', end="")
            i = i+1
            j = j-1
        elif row == col:
            print('*', end="")
        else:
            print(end=" ")
    print()

# M

def getnumber():
 number = input("input number")
 return(number)



def sumdigits():
    n = getnumber()
    n = int(n)
    r = 0
    while n:
        r, n = r + n % 10, n // 10
    return r

sumdigits()

