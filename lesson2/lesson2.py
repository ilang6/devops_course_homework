isTrue = False
a = 4
b = 2.5
strOne = "One"
strThree = "Three"

if a < b :
    print("a is smaller than b")
else :
    print("a is not smaller than b")

if strOne != strThree :
    print("strOne is not strThree")

if strOne != strThree and a < b:
    print("strOne is not strThree")

if a < b:
    print("a is smaller than b")
elif a == b:
    print("a is equal smaller than b")
elif a > b:
    print("a is equal smaller than b")
else :
    print("I dont know who is a and b")

print(a == b)

name = "john"
names = ["john", "wick"]
if name in names :
    print("we found john")

x = [1, 2, 3]
y = [1, 2, 3]

print(x == y)
print(x is y)

if strOne != strThree and a < b:
    print("strOne is not strThree")

number = 10
second_number = 10
first_array = ["4"]
second_array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

if number > 15:
    print("1")

if first_array:
    print("2")

if len(second_array) == 10:
    print("3")

if len(first_array) + len(second_array) == 11:
    print("4")

if first_array and first_array[0] == 1:
    print("5")

if not second_number:
    print("6")

primes = [2, 3, 5, 7]
for prime in primes:
    print(prime)

count = 0
while count < 5:
    print(count)
    count += 1


primes = [2, 3, 5, 7]
for prime in primes:
    print(prime)
    if prime == 3:
        break
else:
    print("loop ended")

lower = 1
upper = 240

for num in range(lower, upper + 1):
    if num > 1:
        for i in range(2, num):
            if (num % i) == 0 or num == 239:
                break
        else:
            print(num)


def square(number):
    return number*number


result = square(7)
print(result)

DogPrefix = "Dog bark sounds like: "
Mynum = input("enter a number")


def bark():
    print(DogPrefix + "Whoof! Whoof!")


def miao(catsound="Miao! Miao!"):
    return print(catsound)


def makeasound(sound):
    sound()


miao("Hatoola")
makeasound(bark)
makeasound(miao)

names = ["Haim", "Ilan" ,"Noam"]
for name in names:
    print(name)

input_from_user = input("please enter your name ?")
print("your name is: "+input_from_user)

if input_from_user not in names:
    print ("GET OUT !!!!")
else:
    print ("Welcome !!!")

thistuple = ("apple", "banana", "cherry")
print(thistuple[1])

my_dict = {"name": "Ilan",
           "hobbies": "python and devops",
           "age": 34}

print(list(my_dict.values()))
