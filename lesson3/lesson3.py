import datetime as dt
import requests as req
# from time import sleep
print(dt.datetime.now())

response = req.get("https://www.ynet.co.il")
if response.status_code == 200:
    print("YNET is live")


def pref(funcation_to_run):
    def wrapper():
        print(dt.datetime.now())
        funcation_to_run()
        print(dt.datetime.now())
    return wrapper

@pref
def a():
    print("here we go")

a()
